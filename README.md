GWSubtract
===========================

A package to effectively subtract linearly correlated noise
from gravitational-wave strain data. 

Installation 
----------

This package has a strong dependency on the `pycbc` package and `lalsuite`.
To install both, first run

```
pip install "numpy>=1.13.0" matplotlib==2.1.0 python-cjson Cython decorator
```

and now you can directly install the packages:

```
pip install lalsuite pycbc==1.9.1
```

Note this package has only been tested with pycbc v1.9.1.

A number of the plotting codes reply upon `gwpy`. This can be installed via:

```
pip install gwpy
```

In order to run workflows and access the segment database, you will also need to run

```
pip install http://download.pegasus.isi.edu/pegasus/4.8.1/pegasus-python-source-4.8.1.tar.gz dqsegdb M2Crypto
```

Now run the installation script via

```
python setup.py install
```

Available Installation at CIT
----------

If you are running on the LCG at CIT, an environment is available via

```
source /home/derek.davis/src/clean_env/bin/activate
```

