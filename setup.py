import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gwsubtract",
    version="0.1.0",
    author="Derek Davis",
    author_email="derek.davis@ligo.org",
    description="A package to subtract noise from gw strain data",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2.7",
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        "Operating System :: OS Independent",
    ],
    scripts  = [
               'bin/gwsubtract_make_crosstalk_cleaning_workflow',
               'bin/gwsubtract_plot_inner_product',
               'bin/gwsubtract_plot_tf',
               'bin/gwsubtract_subtract_noise',
               'bin/gwsubtract_merge_tf',
               'bin/gwsubtract_plot_percentile_spectrum',
               'bin/gwsubtract_project_noise_from_tf',
               'bin/gwsubtract_tf_measure'
    ],
)
